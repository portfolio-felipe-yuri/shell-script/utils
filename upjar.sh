#!/bin/bash

PASS=$1
HOSTS="192.168.0.219 10.50.35.51 10.50.35.52 10.50.35.53 10.50.35.54"

echo -e "Atualizando jars nos servidores 219, 51, 52, 53 e 54"

copyToHost() {
    for HOST in ${HOSTS[@]}; do
        echo "Enviando para $HOST..."
        sshpass -p "$PASS" scp snk-ant.jar skw-environment.jar hudson@$HOST:/home/hudson/.ant/lib

        if [ $? -eq 0 ]; then
            echo -e "\n[$HOST] Arquivos enviados com sucesso!\n"
        else
            echo -e "\n[$HOST] Não foi possível enviar os arquivos!\n"
        fi

    done
}

copyToHost
