#!/bin/bash

gw=$( ip a s tun0 | awk '/inet.*brd/ {print $2}' | awk -F'/' '{print $1}')

if [[ -z $gw ]]
then
	echo "Não encontrei o IP da VPN";
    exit 1
fi

echo "Testando"
sudo ip route add default via 192.168.15.1 #IP do seu modem

sudo ip route add 192.168.0.124/32 via $gw
sudo ip route add 192.168.0.123/32 via $gw
sudo ip route add 192.168.0.215/32 via $gw
sudo ip route add 192.168.1.219/32 via $gw
sudo ip route add 192.168.0.219/32 via $gw
sudo ip route add 192.168.0.199/32 via $gw
sudo ip route add 192.168.1.218/32 via $gw
sudo ip route add 192.168.0.193/32 via $gw
sudo ip route add 192.168.1.187/32 via $gw
sudo ip route add 192.168.1.176/32 via $gw
sudo ip route add 18.231.77.8/32 via $gw
sudo ip route add 10.50.35.29/32 via $gw
sudo ip route add 54.94.57.171/32 via $gw
sudo ip route add 10.50.35.84/32 via $gw
sudo ip route add 52.67.249.64/32 via $gw

#temporario
sudo ip route add 192.168.3.114/32 via $gw