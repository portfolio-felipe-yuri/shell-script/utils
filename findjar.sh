#! /bin/bash


if [ "" == "$1" ]; then
    echo -e "No arguments have been filled.\n Try this: \n\tfjar path-name any-filter \n\t or \n\tfjar any-filter "
    exit 0
fi

if [ "" != "$2" ]; then
    DIR="$1/"
    FILTER=$2
else
    DIR=''
    FILTER=$1
fi

findClass() {
    COUNT=0
    DIR=$DIR*.jar

    for FILE in $DIR; do
        OUTLINE=$(zip -sf $FILE | grep -iE $FILTER | grep -iE .class)
        if [ ! -z "$OUTLINE" ]; then
            echo -e "\nFile name: $FILE \n $OUTLINE \n\n"
            COUNT=$((COUNT + 1))
        fi
    done

    if [ $COUNT -eq 0 ]; then
        echo "No jar found for this filter."
    fi
    exit 0
}

findClass
